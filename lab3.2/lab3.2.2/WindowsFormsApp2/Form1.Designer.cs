﻿namespace WindowsFormsApp2
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_rows_myArr = new System.Windows.Forms.Label();
            this.label_column_myArr = new System.Windows.Forms.Label();
            this.numrows = new System.Windows.Forms.NumericUpDown();
            this.numcolumn = new System.Windows.Forms.NumericUpDown();
            this.run_button = new System.Windows.Forms.Button();
            this.RandView = new System.Windows.Forms.DataGridView();
            this.sumel = new System.Windows.Forms.DataGridView();
            this.max_sum = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.zsuv = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.numrows)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numcolumn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RandView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sumel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zsuv)).BeginInit();
            this.SuspendLayout();
            // 
            // label_rows_myArr
            // 
            this.label_rows_myArr.AutoSize = true;
            this.label_rows_myArr.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_rows_myArr.Location = new System.Drawing.Point(35, 31);
            this.label_rows_myArr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_rows_myArr.Name = "label_rows_myArr";
            this.label_rows_myArr.Size = new System.Drawing.Size(261, 27);
            this.label_rows_myArr.TabIndex = 0;
            this.label_rows_myArr.Text = "Кількість рядків масиву:\r\n";
            // 
            // label_column_myArr
            // 
            this.label_column_myArr.AutoSize = true;
            this.label_column_myArr.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_column_myArr.Location = new System.Drawing.Point(391, 31);
            this.label_column_myArr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_column_myArr.Name = "label_column_myArr";
            this.label_column_myArr.Size = new System.Drawing.Size(284, 27);
            this.label_column_myArr.TabIndex = 1;
            this.label_column_myArr.Text = "Кількість стовпців масиву:\r\n";
            this.label_column_myArr.Click += new System.EventHandler(this.Label_column_myArr_Click);
            // 
            // numrows
            // 
            this.numrows.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numrows.Location = new System.Drawing.Point(311, 28);
            this.numrows.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numrows.Name = "numrows";
            this.numrows.Size = new System.Drawing.Size(48, 35);
            this.numrows.TabIndex = 2;
            this.numrows.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // numcolumn
            // 
            this.numcolumn.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numcolumn.Location = new System.Drawing.Point(691, 28);
            this.numcolumn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numcolumn.Name = "numcolumn";
            this.numcolumn.Size = new System.Drawing.Size(48, 35);
            this.numcolumn.TabIndex = 3;
            this.numcolumn.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // run_button
            // 
            this.run_button.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.run_button.Location = new System.Drawing.Point(825, 31);
            this.run_button.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.run_button.Name = "run_button";
            this.run_button.Size = new System.Drawing.Size(184, 39);
            this.run_button.TabIndex = 4;
            this.run_button.Text = "Run\r\n\r\n";
            this.run_button.UseVisualStyleBackColor = true;
            this.run_button.Click += new System.EventHandler(this.Run_button_Click);
            // 
            // RandView
            // 
            this.RandView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.RandView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.RandView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.RandView.Location = new System.Drawing.Point(32, 129);
            this.RandView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.RandView.Name = "RandView";
            this.RandView.RowHeadersWidth = 51;
            this.RandView.Size = new System.Drawing.Size(977, 151);
            this.RandView.TabIndex = 5;
            // 
            // sumel
            // 
            this.sumel.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.sumel.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.sumel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sumel.Location = new System.Drawing.Point(32, 325);
            this.sumel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.sumel.Name = "sumel";
            this.sumel.RowHeadersWidth = 51;
            this.sumel.Size = new System.Drawing.Size(977, 151);
            this.sumel.TabIndex = 6;
            // 
            // max_sum
            // 
            this.max_sum.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.max_sum.Location = new System.Drawing.Point(239, 495);
            this.max_sum.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.max_sum.Name = "max_sum";
            this.max_sum.Size = new System.Drawing.Size(396, 35);
            this.max_sum.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(35, 89);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(444, 27);
            this.label3.TabIndex = 10;
            this.label3.Text = "Заповнення масиву рандомними числами:\r\n";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.label2.Location = new System.Drawing.Point(35, 295);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(272, 27);
            this.label2.TabIndex = 11;
            this.label2.Text = "Суми елементів стовпців:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(35, 498);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(188, 27);
            this.label1.TabIndex = 12;
            this.label1.Text = "Найбільша з сум:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.label4.Location = new System.Drawing.Point(35, 543);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(149, 27);
            this.label4.TabIndex = 14;
            this.label4.Text = "нова матриця";
            // 
            // zsuv
            // 
            this.zsuv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.zsuv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.zsuv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.zsuv.Location = new System.Drawing.Point(32, 572);
            this.zsuv.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.zsuv.Name = "zsuv";
            this.zsuv.RowHeadersWidth = 51;
            this.zsuv.Size = new System.Drawing.Size(977, 151);
            this.zsuv.TabIndex = 13;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 759);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.zsuv);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.max_sum);
            this.Controls.Add(this.sumel);
            this.Controls.Add(this.RandView);
            this.Controls.Add(this.run_button);
            this.Controls.Add(this.numcolumn);
            this.Controls.Add(this.numrows);
            this.Controls.Add(this.label_column_myArr);
            this.Controls.Add(this.label_rows_myArr);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.numrows)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numcolumn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RandView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sumel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zsuv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_rows_myArr;
        private System.Windows.Forms.Label label_column_myArr;
        private System.Windows.Forms.NumericUpDown numrows;
        private System.Windows.Forms.NumericUpDown numcolumn;
        private System.Windows.Forms.Button run_button;
        private System.Windows.Forms.DataGridView RandView;
        private System.Windows.Forms.DataGridView sumel;
        private System.Windows.Forms.TextBox max_sum;
        private System.Windows.Forms.Label label3;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView zsuv;
    }
}

