﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3._2._1
{
    class Program
    {

            static void shiftMatrixByK(double[,] myArr,
                                         int m, int n)
            {


                int j = 0;
                int i = 0;
                while (i < n / 2)
                {


                    for (i = 0; i < n; i++)
                        for (j = 0; j < m; j++)
                        {
                            Console.Write($" {myArr[n - i - 1, j]}         ");
                        }
                    i += 1;
                    j += 1;

                }
                Console.WriteLine();
            }
            public static void Main(string[] args)
        {
            // Обявляємо двовимірний масив.
            int n, m, k;

            Console.Write("Введiть кiлькiсть рядкiв - n елементiв масиву: ");
            while (!Int32.TryParse(Console.ReadLine(), out n))
            {
                Console.Write("Неправильний ввiд даних n, будь ласка спробуйте ще раз: ");
            }
            Console.Write("Введiть кiлькiсть стовпцiв - m елементiв масиву: ");
            while (!Int32.TryParse(Console.ReadLine(), out m))
            {
                Console.Write("Введiть кiлькiсть рядкiв - m елементiв масиву: ");
            }
            double[,] myArr = new double[n, m];
            // Обявляємо масив який буде відображати максимальну суму cтовпців.
            double[] sumRow = new double[n];


            Console.WriteLine();

            //Створюємо клас Random для заповнення значень елементів масиву рандомними числами.
            Random ran = new Random();

            // Ініціалізуємо масив і заповнюємо його рандомними числами в діапазоні[-42.312;7.003]
            Console.WriteLine("*************************************");
            Console.WriteLine();
            Console.WriteLine("Масив заповнено рандомними числами...");
            Console.WriteLine();
            Console.WriteLine("*********************************************************");
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    myArr[i, j] = ran.NextDouble() * (42.312 + 7.003) - 42.312;
                    //Формула для типу Double заповнення масиву рандомними числами.

                    Console.Write($"{myArr[i, j]}   ");
                    //Виводимо на екран елеминти масиву MyArr
                }
                Console.WriteLine();

            }
            Console.WriteLine();
            Console.WriteLine("*********************************************************");
            Console.WriteLine();
            Console.WriteLine("Сума елементiв кожного стовпця");
            Console.WriteLine();
            Console.WriteLine("********************************");
            for (int j = 0; j < n; j++)
            {
                double sum = 0;

                for (int i = 0; i < m; i++)
                {
                    sum += myArr[i, j];

                }
                sumRow[j] = sum;

                Console.WriteLine($"sumRow[{j}] = {sum}");
            }
            int max_j = 0;
            double max = 0;
            for (int j = 0; j < n; j++)

                if (sumRow[j] > max)
                {
                    max = sumRow[j];
                    max_j = j;
                }
            Console.WriteLine("********************************");
            Console.WriteLine();
            Console.WriteLine("Максимальна сума = {0}", max);
            Console.WriteLine("sumRow: {0}", max_j + 1);
            Console.WriteLine();
            Console.WriteLine("********************************");
            Console.WriteLine();
            Console.WriteLine("нова матриця");
            Console.WriteLine("********************************");
            Console.WriteLine();

            shiftMatrixByK(myArr, n, m);
            Console.WriteLine();
            Console.WriteLine("*********************************************************");
            Console.ReadKey();
        }
    }
}
