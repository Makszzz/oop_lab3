﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3._1._1
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            Console.Write("Введiть кiлькiсть елементiв массиву: ");
            double s = 0, temp, min;
            int size = int.Parse(Console.ReadLine());
            double[] Arr = new double[size];
            Random rand = new Random();
            Console.WriteLine("Згенерований масив:");
            for (int i = 0; i < size; i++)
            {
                Arr[i] = rand.NextDouble() * (53.44 + 10.34) - 10.34;
                Console.WriteLine("{0:f2}", Arr[i]);
            }
            for (int i = 0; i < size; i++)
            {
                if ((Arr[i] % 1 < 0.5) && (Arr[i] > 0))
                { s = s + Arr[i]; }

            }

            Console.WriteLine("Сума модулів елементів які мають дробову частину менше за 0: {0:f2}", s);

            Console.WriteLine("Вiдсортований масив:");
            min = Arr[0];
            for (int i = 1; i < size; i++)
            {
                if (Arr[i] < min)
                {
                    min = Arr[i];
                }
            }
            Console.WriteLine("{0:f2}", min);


            Array.Sort(Arr);
            for (int i = 1; i < size; i++)
                Array.Reverse(Arr);
            for (int i = 0; i < size - 1; i++)
            {
                Console.WriteLine("{0:f2}", Arr[i]);
            }

            Console.WriteLine("Програму успiшно завершено, натиснiть будь яку клавiшу для виходу з програми...");
            Console.ReadKey();
        }
    }
}
