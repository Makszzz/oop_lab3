﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            

        }
        private void Button1_Click(object sender, EventArgs e)
        {
            double temp,min, s=0; 
            int size = Convert.ToInt32(numericUpDown1.Value);
            double[] Arr = new double[size];
            dataGridViewArray.RowCount = 1;
            dataGridViewArray.ColumnCount = size;
            Random rnd = new Random();
            for (int i = 0; i < size; i++)
            {
                Arr[i] = rnd.NextDouble() * (53.44 + 10.34) - 10.34;
                dataGridViewArray[i, 0].Value = Arr[i];
                dataGridViewArray.Columns[i].HeaderText = i.ToString();

            }
            for (int i = 0; i < size; i++)
            {
                if ((Arr[i] % 1 < 0.5) && (Arr[i] > 0))
                { s = s + Arr[i]; }
            }
            textBoxSum.Text = s.ToString("0.000");
            min = Arr[0];
            for (int i = 1; i < size; i++)
            {
                if (Arr[i] < min)
                {
                    min = Arr[i];
                }
            }
            Console.WriteLine("{0:f2}", min);


            Array.Sort(Arr);
            for (int i = 1; i < size; i++)
                Array.Reverse(Arr);
            for (int i = 0; i < size; i++)
            {
                dataGridSort.ColumnCount = size;
                dataGridSort.Rows[0].Cells[i].Value = Arr[i];
            }
               
             

        }

        private void dataGridViewArray_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }


}